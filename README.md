# Build

To build executables simply run `$ make fit` within this folder. It will produce
3 executables "./fit", "./checkmat", and "./guod" described below.

# Introduction

In the paper we deal with a so called "Cell Model" defined by a set of transition
probabilities between different states of a system. Such set of probabilities can
be represented via a row-stochastic matrix B:
- B<sub><em>ij</em></sub> &isin; [0;1]
- &forall; <em>i</em>: &sum;<sub><em>j</em></sub> B<sub><em>ij</em></sub> = 1

Another requirement of the model (equilibrium steady state) implies a symmetry of a matrix
- B<sub><em>ij</em></sub> = B<sub><em>ji</em></sub>

We also require spatial symmetries of the system
- &forall; &sigma;&isin;D<sub><em>n</em></sub>:  
  B(X &rarr; Y |X) = B(&sigma;(X) &rarr; &sigma;(Y) |&sigma;(X))
where Dih<sub><em>n</em></sub> denotes a <a href="https://en.wikipedia.org/wiki/Dihedral_group">dihedral group</a>
(a group of symmetries of an <em>n</em>-gon).

And finally one also requires "transition locality" which effectively fixes some
of the matrix elements to zero.

The softwere presented here provides a way to produce matrices that satisfy the
conditions abowe plus optional extra requirements like "strong locality" presented
an the paper.

# Numerical results
One can find numerical values for matrices presented in the paper in Fig. 2 in
the folder [./matrices](./matrices).
- [matrices/a-uniform](matrices/a-uniform): The matrix which leads to equilibrium violating transport and strong locality.
- [matrices/b-unioffdiag](matrices/b-unioffdiag): The matrix which leads to equilibrium obeying transport locality and violating strong locality.
- [matrices/c-localequilib](matrices/c-localequilib): The matrix which leads to non-equilibrium steady state obeying transport and strong locality.
- [matrices/d-icm](matrices/d-icm): The matrix which leads to equilibrium obeying transport and strong locality.

Within each folder lies a file named "matrix.txt" containing numerical values for each entry of corresponding matrix.
**Note**: some matrices are stored in hexademical format. To read these values one can use `strtod()` function (C/C++).

Some folders (for matrices that were fitted) also contain matrix parameterization which can be provided to a `./fit`-executable.
Numerical values for a non-equilibrium steady state we obtained in one of the models is also provided ([matrices/c-localequilib/steady-state.txt](matrices/c-localequilib/steady-state.txt)).


# The algorithm

In brief, the idea is the fllowing:
* We begin with an exact parameterization for a row-stochastic matrix satisfying spatial symmetries and transition locality:
  * Row wise: each row can be though of as a set of probablities P(X &rarr; Y | X) for some given X (Y running over all other microstates). 
    * due to transition locality some entries in a row are fixed to zero;
    * there are entries in a row that are required to be equal due to spatial symmetries, e.g.:  
      if &sigma;(X) = X for some &sigma; &isin; Dih<sub><em>n</em></sub>, then elements of the row corresponding to P(X &rarr; Y | X) and P(X &rarr; &sigma;(Y) | X) must be equal;
    * let <em>n</em> be a number of independent non-zero entries in the row; due to normalization requirement the row can be parameterized by <em>n</em>-1 variables.
  * Now, consider a row corresponding to a state X. Pick some permutation &sigma; &isin; Dih<sub><em>n</em></sub>. Using the fact that P(X &rarr; Y | X) = P(&sigma;(X) &rarr; &sigma;(Y) | &sigma;(X)) we see that by fixing values in the X-row we autmatically fix all the values in the &sigma;(X)-row.
  * Thus the final number of free parameters can be obtained by running over the set of rows from a different Dih<sub><em>n</em></sub>-equivalence classes, and summing number of free parameters in each of these rows.
* Matrix symmetry: <b>TODO</b>
* Strong locality: <b>TODO</b>
  

<!--The problem can be formulated as follows:-->
<!--- produce a bistochastic matrix (row-stochastic + symmetry = bistochastic)-->

<!--Size of the matrix we are typically dealing with is 126x126 elements. It-->
<!--prevents us from exploiting Birkhoff–von Neumann decomposition (any bistochastic-->
<!--matrix can be represented as a weighted sum of permutation matrices) as to span-->
<!--all possible matrices of our interest we would need to work in decomposition. Neither we-->
<!--can exploit the <a href="https://arxiv.org/abs/1106.1925">Sinkhorn propagation</a>-->
<!--since there are zero-valued entries in the matrix. All of the above forces us to-->
<!--seek for an Ad hoc approach to obtain a bistochastic matrix.-->

<!--Unfortunately there is no known "analytical" way to produce a matrix satisfying-->
<!--all the conditions we imply with -->

# ./fit

```
usage: ./fit -N <balls> [OPTIONS]

Read/write matrix parameterization:
  --input  -i <path>   Read parameters from specified file.
  --output -o <path>   Write parameters to specified file after the fit.
  --update -u <path>   Combination of the two options above.

Loss function definition:
  --loss-def         <type>    Specify a strategy to minimize correlations. Values must be one of:
                               - rx-sum -- Minimize Rx for supplied definitions of measures.
                               - corr-func -- Minimize factorization of probabilities directly (default).
                               - gauss(<sigma>)
                               - cup(<sigma>)
  --sym-def       -s <type>    Specify implementation of symmetry constrain. Value must be one of:
                               - sum-of-squares
                               - sqrt-sum-of-squares
                               - sum-of-modules
                               - gauss(<sigma>)
                               - cup(<sigma>)
  --matrix-def=spatsym         Specify matrix type:
                               - default
                               - spatsym
                               - human
                               - spatsym-human
                               - spatsym-human-simp(<max-change>)
                               - spatsym-human-noint(<max-change>)
  --measure       -m <type>[x<n>]  Add a measure specified by type in the loss function.
                               Available values are:
                               - f1 -- From paper: 1/(1 + n_u^I + n_u^F)
                               - f2 -- From paper: 1/(1 + n_u^I) + 1/(1 + n_u^F)
                               - full -- Generate random graph of function f(X, n_u).
                               - mult -- Generate random graph of function f(n_u, n_u).
                               In the last two cases separate function is generated for each cell to be measured.
  --cells-pair    -p <first>,<second>|'all'
  --all-cells                  Measure correlations for all separated pairs of cells.
  --rxterm-scale  -R <scale>   Scale Rx-term of the loss function.
  --symterm-scale -S <scale>   Scale symmetry-term of the loss function.

Fit configuration:
  --cycles      -c <n=2>
  --interactive -I
  --cmd <command>

Miscelenious:
  --error-scale -E <x>
  --bnd1 <x>
  --bnd2 <x>
  --sval <val>,<err>
  --write-matrix   <path>
  --load-matrix    <path>
```


# ./checkmat

# ./guod
