#include <iostream>
//#include <optional>
#include <fstream>
#include <cstring>
#include <cassert>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <stdexcept>
#include <cstdlib>
#include <climits>

#include <getopt.h>

#include "boson/models.hpp"
#include "boson/matrix.hpp"
#include "gnuplot.hpp"

#include <TCanvas.h>
#include <TH1D.h>
#include <TGraph.h>
#include <TApplication.h>
#include <TLine.h>
#include <TLegend.h>

typedef long double REAL;

template <typename T>
inline void
copy_matrix(T *dst, int m, int n, const T *src)
{ std::memcpy(dst, src, sizeof(T)*m*n); }

template <typename T>
void
matmult(T *C, int m, int n, const T *A, int o, int p, const T *B)
{
  assert(n == o);
  for (int i = 0; i < m; ++i)
  {
    for (int j = 0; j < p; ++j)
    {
      T sum = 0;
      for (int k = 0; k < n; ++k)
      {
        const T aik = A[n*i+k];
        const T bkj = B[p*k+j];
        sum += aik*bkj;
      }
      C[i*p+j] = sum;
    }
  }
}

template <typename T>
struct results {
  T mean_theo, mean_real, stddev_theo, stddev_real;
};

template <typename T>
results<T>
test_matrix(int n, const T *M)
{
  const REAL mean_theo = 1./n;
  T mean_real = 0;
  for (int i = 0; i < n; ++i)
  {
    for (int j = 0; j < n; ++j)
    {
      const T bij = M[i*n+j];
      mean_real += bij;
    }
  }
  mean_real = mean_real/(n*n);
  T stddev_theo = 0, stddev_real = 0;
  for (int i = 0; i < n; ++i)
  {
    for (int j = 0; j < n; ++j)
    {
      const T bij = M[i*n+j];
      stddev_theo += (mean_theo - bij)*(mean_theo - bij);
      stddev_real += (mean_real - bij)*(mean_real - bij);
    }
  }
  stddev_theo = sqrt(stddev_theo/n);
  stddev_real = sqrt(stddev_real/n);

  return { mean_theo, mean_real, stddev_theo, stddev_real };

  //std::cout << "> theo: mean=" << mean_theo << ", std.dev.=" << stddev_theo << std::endl;
  //std::cout << "> real: mean=" << mean_real << ", std.dev.=" << stddev_real << std::endl;
  //std::cout << "> theo - real: " << std::fabs(mean_theo - mean_real) << std::endl;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Produce a table of the form:
 *
 * (final mult @ `cell`, 3-cell-substate) -> [(Xidx, P(n^{fin}_{cell})), ...*]
 *                                       ...
 */
template <typename T>
void
calc_mult_probs(int n, const T *M, const bos::cellular_universe &model,
    const int cell, bos::mult_prob_table &out)
{
  const int ncells = model.n_cells();
  const std::vector<int> adjcells = {
    (cell + ncells-1) % ncells, cell, (cell + 1) % ncells
  };
  std::vector<int> buf (3);

  // for each microstate `X`
  for (int xidx = 0; xidx < n; ++xidx)
  {
    const bos::state &x = model.ordered_states()[xidx];

    // extract substate arround `cell`
    for (int i = 0; i < buf.size(); ++i)
      buf[i] = x[adjcells[i]];
    bos::state substate {buf};

    // calculate probabilites to get n^fin balls in final state given `X`
    double pnfin[model.n_balls()+1];
    std::fill(pnfin, pnfin+model.n_balls()+1, 0);
    for (int yidx = 0; yidx < n; ++yidx)
    {
      const bos::state &y = model.ordered_states()[yidx];
      pnfin[y[cell]] += M[xidx*n+yidx];
    }

    // record this probabilites
    bos::mult_prob_marker mark {0, substate};
    for (int nfin = 0; nfin <= model.n_balls(); ++nfin)
    {
      if (pnfin[nfin] != 0)
      {
        mark.nfin = nfin;
        out[mark].emplace_back(xidx, pnfin[nfin]);
      }
    }
  }
}

template <typename T>
void
calc_mult_probs_extra(int n, const T *M, const bos::cellular_universe &model,
    const int cell, bos::mult_prob_table &out)
{
  const int ncells = model.n_cells();
  const std::vector<int> adjcells = {
    (cell + ncells-1) % ncells, cell, (cell + 1) % ncells
  };
  std::vector<int> buf (3);

  for (int xidx = 0; xidx < n; ++xidx)
  {
    const bos::state &x = model.ordered_states()[xidx];

    // extract substate arround `cell`
    for (int i = 0; i < buf.size(); ++i)
      buf[i] = x[adjcells[i]];
    bos::state substate {buf};

    for (int ntp1 = 0; ntp1 <= model.n_balls(); ++ntp1)
    {
      // calculate probabilites to get n^fin balls in final state given X
      // (conditioned on n^{t+1} balls)
      double pnorm = 0; // must normalize conditioned probabilites
      double pnfin[model.n_balls()+1];
      std::fill(pnfin, pnfin+model.n_balls()+1, 0);
      for (int yidx = 0; yidx < n; ++yidx)
      {
        const bos::state &y = model.ordered_states()[yidx];
        // condition on multiplicity in distant cell at (t+1)
        if (y[(cell+3) % model.n_cells()] != ntp1)
          continue;
        pnorm += M[xidx*n+yidx];
        pnfin[y[cell]] += M[xidx*n+yidx];
      }
      // record this probabilites
      bos::mult_prob_marker mark {0, substate};
      for (int nfin = 0; nfin <= model.n_balls(); ++nfin)
      {
        if (pnfin[nfin] != 0)
        {
          pnfin[nfin] = pnfin[nfin]/pnorm;
          mark.nfin = nfin;
          out[mark].emplace_back(xidx, pnfin[nfin]);
        }
      }
    }
  }
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * "Fold" the table created by `calc_mult_probs()`. If "ghostly action at a
 * distance" is absent, then this function will return zero.
 */
static double
V_SL(const bos::mult_prob_table &mptab)
{
  /*
  double ret = 0;
  for (const auto &[_, probs] : mptab) // for each substate and n^fin
  {
    double mean = 0;
    for (const auto& [_, p] : probs)
      mean += p;
    mean /= probs.size();

    for (const auto &[_, p] : probs)
      ret += (p/mean - 1)*(p/mean - 1);
  }
  return ret;
  // */

  double ret = 0;
  int n = 0;
  for (const auto &[_, probs] : mptab) // for each substate and n^fin
  {
    double mean = 0;
    for (const auto& [_, p] : probs)
      mean += p;
    mean /= probs.size();

    for (const auto &[_, p] : probs)
    {
      ret += (p - mean)*(p - mean);
      n ++;
    }
  }
  return ret/n;
}

struct orbit {
  static REAL default_epsilon;

  static void
  set_default_epsilon(REAL epsilon)
  { default_epsilon = epsilon; }

  static REAL
  get_default_epsilon()
  { return default_epsilon; }

  REAL value;
  REAL epsilon;

  explicit
  orbit(REAL _value, REAL _epsilon)
  : value {_value}, epsilon {_epsilon}
  { }

  orbit(REAL _value)
  : value {_value}, epsilon {default_epsilon}
  { }
  bool
  operator == (const orbit &other) const noexcept
  { return std::fabs(value - other.value) <= std::max(epsilon, other.epsilon); }
};
REAL orbit::default_epsilon = 1E-10;

namespace std {
  template <>
  struct hash<orbit> {
    size_t
    operator () (const orbit &x) const noexcept
    { return std::hash<REAL>()(x.value); }
  };
}

enum class evo { onestep, pow2 };

int
main(int argc, char **argv)
{
  option longopts[] = {
    {"help", false, nullptr, 'h'},
    {"plot", true, nullptr, 'p'},
    {"mode", true, nullptr, 'm'},
    {"batch-mode", false, nullptr, 'b'},
    {"output", true, nullptr, 'o'},
    {0,0,0,0}
  };

  int nevolve = 100;
  int nballs = -1;
  std::string plotpath;
  evo evomode = evo::pow2;
  bool batchmode = false;
  std::string opath;

  int opt;
  while ((opt = getopt_long(argc, argv, "he:N:p:m:b:o:", longopts, NULL)) > 0)
  {
    switch (opt)
    {
      case 'h':
        printf("usage: %s -N <n-balls> [OPTIONS]\n", argv[0]);
        printf("\n");
        printf("OPTIONS:\n");
        printf("  --help -h              Show this message and exit.\n");
        printf("  --plot -p <plot-path>  Specify path for evolution plots.\n");
        printf("  --mode -m <mode>       Evolution mode: 'onestep' or 'pow2'.\n");
        printf("  --batch-mode -b        Calculate numbers and exit; do not show matrices.\n");
        printf("  --output -o <path>     Write numerical values of a steady state distribution to a file.\n");
        exit(EXIT_SUCCESS);

      case 'o':
        opath = optarg;
        break;

      case 'e':
        nevolve = atoi(optarg);
        break;

      case 'N':
        nballs = atoi(optarg);
        break;

      case 'p':
        plotpath = optarg;
        break;

      case 'm':
        if (strcasecmp(optarg, "onestep") == 0)
          evomode = evo::onestep;
        else if (strcasecmp(optarg, "pow2") == 0)
          evomode = evo::pow2;
        else
        {
          std::cout << "> error: evolution mode, " << optarg << std::endl;
          return EXIT_FAILURE;
        }
        break;

      case 'b':
        batchmode = true;
        break;

      default:
        std::cout << "> error: undefined option, " << argv[optind-1] << std::endl;
        return EXIT_FAILURE;
    }
  }

  if (nballs < 0)
  {
    std::cerr << "> error: number of balls not specified" << std::endl;
    return EXIT_FAILURE;
  }
  if (optind == argc)
  {
    std::cerr << "> error: no matrix provided" << std::endl;
    return EXIT_FAILURE;
  }
  const std::string mpath = argv[optind];

  const bos::cellular_universe &model = bos::select_model(nballs);

  //////////////////////////////////////////////////////////////////////////////
  //                             READ MATRIX
  //
  const int n = model.states().size();
  REAL B[n][n];
  std::cout << "> reading matrix" << std::endl;
  std::ifstream matfile {mpath};
  std::string buf;
  for (int i = 0; i < n; ++i)
  {
    for (int j = 0; j < n; ++j)
    {
      matfile >> buf;
      B[i][j] = std::strtod(buf.c_str(), nullptr);
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  //                                 GOD
  //
  std::vector<std::vector<std::tuple<int, int, REAL, REAL>>> precepts;
  for (int xidx = 0; xidx < n; ++xidx)
  {
    const bos::state &x = model.ordered_states()[xidx];
    precepts.emplace_back();
    auto &xprec = precepts.back();

    for (int yidx = 0; yidx < n; ++yidx)
    {
      const bos::state &y = model.ordered_states()[yidx];

      if (x[0]!=y[0] or x[1]!=y[1] or x[2]!=y[2])
        continue;

      for (int nfin = 0; nfin <= model.n_balls(); ++nfin)
      {
        REAL px = 0, py = 0;
        for (int zidx = 0; zidx < n; ++zidx)
        {
          const bos::state &z = model.ordered_states()[zidx];
          if (z[1] != nfin)
            continue;
          px += B[xidx][zidx];
          py += B[yidx][zidx];
        }
        xprec.emplace_back(yidx, nfin, px, py);
      }
    }
  }

  std::cout << "> searching for a God..." << std::endl;
  for (int xidx = 0; xidx < n; ++xidx)
  {
    REAL maxdiff = DBL_MIN;
    int maxstate = -1;
    int maxnfin = -1;
    for (const auto [yidx, nfin, px, py] : precepts[xidx])
    {
      //const REAL cmp = std::fabs(px/py-1);
      const REAL cmp = std::fabs(px-py);
      if (cmp > maxdiff)
      {
        maxdiff = cmp;
        maxstate = yidx;
        maxnfin = nfin;
      }
    }
    //if (maxstate >= 0)
      //std::cout << "  Xi=" << model.ordered_states()[xidx] << " -> " << maxdiff
        //<< "(Xj=" << model.ordered_states()[maxstate] << ", n^{fin}=" << maxnfin << ")"
        //<< std::endl;
  }

  //////////////////////////////////////////////////////////////////////////////
  //                        STRONG LOCALITY MEASURE
  //
  bos::mult_prob_table ptab;
  //calc_mult_probs(n, &B[0][0], model, 1, ptab);
  calc_mult_probs_extra(n, &B[0][0], model, 1, ptab);
  std::cout << "> V_SL = " << V_SL(ptab) << std::endl;

  //////////////////////////////////////////////////////////////////////////////
  //                      INFORMATION ENTROPY MEASURE
  //
  REAL entryepsilon = 0;
  for (int i = 0; i < n; ++ i)
  {
    for (int j = i+1; j < n; ++j)
      entryepsilon = std::max(entryepsilon, std::fabs(B[i][j] - B[j][i]));
  }
  orbit::set_default_epsilon(entryepsilon);
  std::cout << "> entry epsilon is set to " << entryepsilon << std::endl;

  std::unordered_map<orbit, int> entrycounts;
  for (int i = 0; i < n; ++ i)
  {
    for (int j = 0; j < n; ++j)
    {
      const double entry = B[i][j];
      entrycounts[entry] += 1;
    }
  }
  double entropy = 0;
  for (const auto &[_, cnt] : entrycounts)
  {
    const double p = double(cnt)/(n*n);
    entropy += -p*log(p);
  }
  std::cout << "> information entropy: " << entropy << std::endl;

  //////////////////////////////////////////////////////////////////////////////
  //                              EVOLUTION
  //
  std::ofstream plotfile;
  if (not plotpath.empty())
    plotfile.open(plotpath);

  REAL buf1[n][n], buf2[n][n];
  copy_matrix(&buf1[0][0], n, n, &B[0][0]);
  REAL *oldacc = &buf1[0][0];
  REAL *newacc = &buf2[0][0];
  std::cout << "> running evolution...";
  std::cout.flush();
  for (int i = 1; i <= nevolve; ++i)
  {

    switch (evomode)
    {
      case evo::onestep: matmult(newacc, n, n, oldacc, n, n, &B[0][0]); break;
      case evo::pow2: matmult(newacc, n, n, oldacc, n, n, oldacc); break;
      default: abort();
    }
    if (plotfile.is_open())
    {
      const REAL stddev = test_matrix(n, newacc).stddev_theo;
      const REAL power = evomode == evo::onestep ? 1 + i : 1ll << i;
      plotfile << power << '\t' << stddev << std::endl;
    }
    std::swap(newacc, oldacc);

    // print progress
    std::cout << "\e[2K\r";
    std::streamsize ss = std::cout.precision();
    std::cout << "> running evolution... "
      << std::fixed << std::setprecision(1)
      << (REAL(i*100)/nevolve) << "%";
    std::cout.unsetf(std::ios::fixed);
    std::cout.unsetf(std::ios_base::floatfield);
    std::cout << std::setprecision(ss);
    std::defaultfloat(std::cout);
    std::cout.flush();
  }
  std::cout << "\e[2K\r";
  std::cout << "> running evolution... done" << std::endl;
  if (plotfile.is_open())
    plotfile.close();

  //////////////////////////////////////////////////////////////////////////////
  //                               STD. DEV.
  //
  {
    const auto [mean_theo, mean_real, stddev_theo, stddev_real] =
      test_matrix(n, newacc);
    std::cout << "> theo: mean=" << mean_theo << ", std.dev.=" << stddev_theo << std::endl;
    std::cout << "> real: mean=" << mean_real << ", std.dev.=" << stddev_real << std::endl;
    std::cout << "> theo - real: " << std::fabs(mean_theo - mean_real) << std::endl;
  }

  //////////////////////////////////////////////////////////////////////////////
  //                                 PLOT
  //
  // distribution of probabilities at steady state
  std::vector<double> xs, ys;
  xs.reserve(n);
  ys.reserve(n);
  for (int i = 0; i < n; ++i)
  {
    xs.push_back(i);
    ys.push_back(newacc[i]);
  }

  // write the distribution
  if (not opath.empty())
  {
    std::string fullpath;
    char buf[PATH_MAX];
    if (realpath(mpath.c_str(), buf))
      fullpath = buf;

    std::ofstream os {opath};
    os << "# Steady state distribution for a matrix '"
      << (fullpath.empty() ? mpath : fullpath) << std::endl;
    os << "# N balls: " << nballs << std::endl;
    os << "# N states: " << n << std::endl;
    os << "#" << std::endl;
    os << std::setw(5) << std::left << "# X" << "\t" << "P(X)" << std::endl;
    for (int i = 0; i < n; ++i)
      os << "  " << std::setw(5) << std::left << xs[i] << "\t" << ys[i] << std::endl;
  }

  // plot the distribution
  if (not batchmode)
  {
    REAL maxp = 0;
    for (int i = 0; i < n; ++i)
      maxp = std::max(maxp, newacc[i]);

    TApplication app {"app", 0, 0, 0, -1};;

    int w = 800, h = 800;
    TCanvas *c = new TCanvas;
    c->SetCanvasSize(800, 800);
    c->SetWindowSize(1010, 800);

    TGraph *g = new TGraph {(int)xs.size(), xs.data(), ys.data()};
    g->SetTitle("");
    g->SetMarkerStyle(8);
    g->SetMarkerColor(9);
    g->SetLineColor(16);
    g->GetXaxis()->SetRangeUser(0, n-1);
    g->GetXaxis()->SetTitle("microstate");
    g->GetXaxis()->SetTitleSize(0.045);
    //g->GetXaxis()->SetTitleOffset(1);
    //g->GetXaxis()->SetLabelSize(0.03);
    //g->GetYaxis()->SetRangeUser(0, maxp*1.05);
    g->GetYaxis()->SetRangeUser(0, 0.02);
    g->GetYaxis()->SetTitle("#tilde{#it{P}}(microstate)");
    g->GetYaxis()->SetTitleSize(0.045);
    //g->GetYaxis()->SetLabelSize(0.03);
    g->GetYaxis()->SetNdivisions(3);

    TLine *l = new TLine {0.0, 1.0/n, double(n-1), 1.0/n};
    l->SetLineStyle(9);
    l->SetLineColor(46);
    l->SetLineWidth(2);

    TLegend *leg = new TLegend {0.7, 0.8, 0.9, 0.9};
    leg->AddEntry(g, "non-equilibrium steady state", "LP");
    leg->AddEntry(l, "equilibrium state", "L");
    leg->SetBorderSize(0);

    g->Draw("ALP");
    l->Draw("LSame");
    leg->Draw("Same");

    c->Update();
    app.Run();

    delete g;
    delete l;
    delete leg;
  }
}
