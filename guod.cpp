#include <climits>
#include <getopt.h>
#include <optional>

#include "boson/models.hpp"

void
generate_matrix(const bos::cellular_universe &model, double poffdiag,
    FILE *os)
{
  const int n = model.states().size();
  for (int xidx = 0; xidx < n; ++xidx)
  {
    const bos::state_set &xperms = model.permutations(xidx);
    for (int yidx = 0; yidx < n; ++yidx)
    {
      const bos::state &y = model.ordered_states()[yidx];
      if (xidx == yidx)
      {
        const double pdiag = 1. - poffdiag*((double)xperms.size() - 1.);
        fprintf(os, " %a", pdiag);
      }
      else if (xperms.find(y) != xperms.end())
        fprintf(os, " %a", poffdiag);
      else
        fprintf(os, " %a", 0.);
    }
    fprintf(os, "\n");
  }
}

int
main(int argc, char **argv)
{
  int nballs = -1;
  int opt;
  std::string opath;
  std::optional<double> poffdiag;
  while ((opt = getopt(argc, argv, "hN:p:o:")) > 0)
  {
    switch (opt)
    {
      case 'h':
        std::cout << "usage: " << argv[0]
          << " -N <n-balls> [-p <p-offdiag> -o <output-path>]" << std::endl;
        return EXIT_SUCCESS;

      case 'N':
        nballs = atoi(optarg);
        break;

      case 'o':
        opath = optarg;
        break;

      case 'p':
        poffdiag = strtod(optarg, nullptr);
        break;

      default:
        std::cerr << "> error: undefined command-line option" << std::endl;
        return EXIT_FAILURE;
    }
  }

  if (nballs < 0)
  {
    std::cerr << "> error: number of balls not specified" << std::endl;
    return EXIT_FAILURE;
  }
  if (not opath.empty() != poffdiag.has_value())
  {
    std::cerr << "> error: missing arguments for matrix generation"
      << std::endl;
    return EXIT_FAILURE;
  }

  const bos::cellular_universe &model = bos::select_model(nballs);
  const int nstates = model.states().size();
  int maxperms = 0, maxpermsidx = 0;
  for (int xidx = 0; xidx < nstates; ++xidx)
  {
    const int nperms = model.permutations(xidx).size();
    if (nperms > maxperms)
    {
      maxperms = nperms;
      maxpermsidx = xidx;
    }
  }

  const double maxpoff = 1./(maxperms - 1);
  const bos::state &x = model.ordered_states()[maxpermsidx];
  std::cout
    << "> max. permutatoins = " << maxperms << " (for state " << x << ")\n"
    << "> max. offdiagonal element = " << maxpoff << std::endl;

  if (poffdiag.has_value())
  {
    if (poffdiag.value() > maxpoff)
      std::cerr << "> warning: offdiagonal element is too large" << std::endl;

    FILE *out = fopen(opath.c_str(), "w");
    if (out == NULL)
    {
      std::cerr << "> error: failed to open output file \"" << opath << "\""
        << std::endl;
      return EXIT_FAILURE;
    }

    generate_matrix(model, poffdiag.value(), out);
    fclose(out);
    std::cout << "> wrote matrix to \"" << opath << "\"" << std::endl;
  }

  return EXIT_SUCCESS;
}
