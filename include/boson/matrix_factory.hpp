#ifndef BOSON_MATRIX_FACTORY_HPP
#define BOSON_MATRIX_FACTORY_HPP

#include "boson/matrix.hpp"

namespace bos {

class matrix_factory {
  public:
  static matrix_factory&
  instance() noexcept
  {
    static matrix_factory factory;
    return factory;
  }

  matrix_base&
  operator () (const std::string &matrixdef, const cellular_universe &model)
    noexcept;

  private:
  matrix_factory() = default;
  matrix_factory(const matrix_factory &) = delete;
  void operator = (const matrix_factory &) = delete;

  ~matrix_factory()
  {
    for (matrix_base *m : m_cache)
      delete m;
  }

  private:
  std::vector<matrix_base*> m_cache;
};

template <typename ...Args>
inline
matrix_base&
build_matrix(Args&& ...args)
{
  matrix_factory &factory = matrix_factory::instance();
  return factory(std::forward<Args>(args)...);
}

} // namespace bos

#endif
