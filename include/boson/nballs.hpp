#ifndef BOSON_NBALLS_HPP
#define BOSON_NBALLS_HPP

#include "boson/states.hpp"
#include "boson/permutations.hpp"

#include <vector>
#include <cmath>

namespace bos {

static double
fac(int n)
{
  double acc = 1;
  for (double i = 1; i <= n; ++i)
    acc *= i;
  return acc;
}

static void
generate_states_n_aux(int ncells, state_set &set, int idx, int nballs,
    std::vector<int> &mult)
{
  if (idx == ncells)
  {
    set.emplace(mult);
  }
  else if (idx == ncells - 1)
  {
    mult[idx] = nballs;
    generate_states_n_aux(ncells, set, idx+1, 0, mult);
  }
  else
  {
    for (int n = 0; n <= nballs; ++n)
    {
      mult[idx] = n;
      generate_states_n_aux(ncells, set, idx+1, nballs-n, mult);
    }
  }
}

static void
generate_states_n(int nballs, int ncells, state_set &set)
{
  std::vector<int> mult (ncells, 0);
  std::cout << "> generating states...";
  std::cout.flush();
  generate_states_n_aux(ncells, set, 0, nballs, mult);
  std::cout << "\e[2K\r";
  std::cout << "> " << set.size() << " states created" << std::endl;
}

class n_balls : public cellular_universe {
  public:
  n_balls(int n_balls, int n_cells)
  : cellular_universe {n_balls}
  { generate_states_n(n_balls, n_cells, m_S); }

  const state_set&
  states() const override
  { return m_S; }

  protected:
  void
  list_permutations(const state &X, state_set &F) const override
  { pick_permutations(m_S, X, F); }

  private:
  state_set m_S;
};

} // namespace bos

#endif
