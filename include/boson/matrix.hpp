#ifndef BOSON_MATRIX_HPP
#define BOSON_MATRIX_HPP

#include "boson/states.hpp"
#include "boson/measure.hpp"
#include "boson/permutations.hpp"
#include "perm_matrix.hpp"

#include <vector>
#include <cmath>
#include <cfloat>

namespace bos {

enum class sym_def {
  sum_of_squares,
  sqrt_sum_of_squares,
  sum_of_modules,
  gauss_of_sum_of_squares,
  cup_of_sum_of_squares,
};

struct row_entry {
  row_entry() = default;
  row_entry(size_t _Yidx, size_t _Xcnt = SIZE_MAX, double _val = DBL_MAX)
  : state_idx {_Yidx}, sym_ent_cnt {_Xcnt}, val {_val}
  { }

  size_t state_idx, sym_ent_cnt;
  double val;
};

typedef std::vector<row_entry> matrix_row;

struct mult_prob_marker {
  int nfin;
  bos::state substate;

  mult_prob_marker(int _mult, const bos::state &_substate)
  : nfin {_mult}, substate {_substate}
  { }

  bool
  operator == (const mult_prob_marker &other) const noexcept
  { return nfin == other.nfin and substate == other.substate; }
};

typedef std::vector<std::pair<int, double>> mult_probs;
typedef std::unordered_map<mult_prob_marker, mult_probs> mult_prob_table;

struct matrix_base {
  matrix_base(const cellular_universe &model)
  : m_model {model},
    m_states {model.ordered_states()}
  { }

  virtual
  ~matrix_base() = default;

  virtual int
  n_rows() const = 0;

  virtual int
  n_ext_parm() const = 0;

  virtual void
  update(const double *extparm) = 0;

  virtual void
  calc_rxs(const bos::measure &fu, const bos::measure &fv,
      std::vector<std::pair<double, double>> &out) const = 0;

  struct symmetry {
    sym_def def;
    double sigma;

    static symmetry
    sum_of_squares() noexcept
    { return symmetry { .def = sym_def::sum_of_squares, .sigma = NAN }; }

    static symmetry
    sqrt_sum_of_squares() noexcept
    { return symmetry { .def = sym_def::sqrt_sum_of_squares, .sigma = NAN }; }

    static symmetry
    sum_of_modules() noexcept
    { return symmetry { .def = sym_def::sum_of_modules, .sigma = NAN }; }

    static symmetry
    gauss_of_sum_of_squares(double sigma) noexcept
    { return symmetry{.def = sym_def::gauss_of_sum_of_squares, .sigma = sigma};}

    static symmetry
    cup_of_sum_of_squares(double sigma) noexcept
    { return symmetry{.def = sym_def::cup_of_sum_of_squares, .sigma = sigma};}
  };

  virtual void
  calc_corrfunc(const std::pair<int, int> &cells,
      std::vector<std::pair<double, double>> &out) const = 0;

  virtual void
  calc_mult_probs(const int cell, mult_prob_table &out) const = 0;

  virtual void
  calc_stronglocty_extra(const int cell, mult_prob_table &out) const = 0;

  virtual double
  symmetry_term() const = 0;

  virtual double
  symmetry_term_abs() const = 0;

  virtual void
  write(FILE *out) const = 0;

  virtual void
  read(std::istream &in) = 0;

  virtual void
  to_ext_parms(std::vector<double> &extparms) const = 0;

  const cellular_universe&
  model() const noexcept
  { return m_model; }

  const std::vector<state>&
  states() const noexcept
  { return m_states; }

  protected:
  const cellular_universe &m_model;
  const std::vector<state> &m_states;
};

class matrix : public matrix_base {
  public:
  matrix(const cellular_universe &model);

  int
  n_rows() const noexcept override
  { return m_rows.size(); }

  int
  row_length(int Xidx) const noexcept
  { return m_rows[Xidx].size(); }

  int
  n_ext_parm(int Xidx) const noexcept
  { return m_rows[Xidx].size() - 1; }

  int
  n_ext_parm() const noexcept override;

  int
  row_entry_to_state_index(int rowidx, int entcnt) const noexcept
  { return m_rows[rowidx][entcnt].state_idx; }

  int
  state_index_to_row_entry(int xidx, int yidx) const noexcept
  {
    const matrix_row &xrow = m_rows[xidx];
    for (int iy = 0; iy < xrow.size(); ++iy)
    {
      if (xrow[iy].state_idx == yidx)
        return iy;
    }
    abort();
  }

  double
  at(int xidx, int yidx) const noexcept
  { return m_rows[xidx][state_index_to_row_entry(xidx, yidx)].val; }

  double
  operator () (int xidx, int yidx) const noexcept
  { return this->at(xidx, yidx); }

  double
  symmetric_entry(int xidx, int ycnt) const noexcept
  {
    const int yidx = m_rows[xidx][ycnt].state_idx;
    const int xcnt = m_rows[xidx][ycnt].sym_ent_cnt;
    return m_rows[yidx][xcnt].val;
  }

  void
  update(const double *extparm) override;

  void
  calc_rxs(const measure &fu, const measure &fv,
      std::vector<std::pair<double, double>> &out) const override;

  void
  calc_corrfunc(const std::pair<int, int> &cells,
      std::vector<std::pair<double, double>> &out) const override;

  void
  calc_mult_probs(const int cell, mult_prob_table &out) const override;

  void
  calc_stronglocty_extra(const int cell, mult_prob_table &out) const override;

  double
  symmetry_term() const override;

  double
  symmetry_term_abs() const override;

  void
  write(FILE *out) const override;

  void
  read(std::istream &in) override;

  void
  to_ext_parms(std::vector<double> &extparms) const override { abort(); }

  private:
  std::vector<matrix_row> m_rows;

  friend class spatsym_matrix;
  friend class sym_matrix;
  friend class hum_matrix;
}; // class bos::matrix

class spatsym_matrix : public matrix_base {
  public:
  spatsym_matrix(const cellular_universe &model);

  int
  n_rows() const noexcept override
  { return m_base.n_rows(); }

  int
  n_ext_parm() const noexcept override
  {
    int n = 0;
    for (const auto &c : m_part)
      n += m_base.m_rows[c[0].first].size();
    return n;
  }

  int
  n_ext_parm(int iclass) const noexcept
  { return m_base.m_rows[m_part[iclass][0].first].size() - 1; }

  void
  update(const double *extparm) override;

  void
  calc_rxs(const bos::measure &fu, const bos::measure &fv,
      std::vector<std::pair<double, double>> &out) const override
  { m_base.calc_rxs(fu, fv, out); }

  void
  calc_corrfunc(const std::pair<int, int> &cells,
      std::vector<std::pair<double, double>> &out) const override
  { m_base.calc_corrfunc(cells, out); }

  void
  calc_mult_probs(const int cell, mult_prob_table &out) const override
  { m_base.calc_mult_probs(cell, out); }

  void
  calc_stronglocty_extra(const int cell, mult_prob_table &out) const override
  { m_base.calc_stronglocty_extra(cell, out); }

  double
  symmetry_term() const override
  { return m_base.symmetry_term(); }

  double
  symmetry_term_abs() const override
  { return m_base.symmetry_term_abs(); }

  void
  write(FILE *out) const override
  { m_base.write(out); }

  void
  read(std::istream &in) override
  { m_base.read(in); }

  void
  to_ext_parms(std::vector<double> &extparms) const override;

  private:
  matrix m_base;
  partition m_part;
  std::vector<std::vector<int>> m_gymap;
  const std::unordered_map<state, int> &m_idxmap;
};

class hum_matrix : public matrix_base {
  public: 
  hum_matrix(const cellular_universe &model);

  virtual
  ~hum_matrix() = default;

  int n_rows() const noexcept override
  { return m_base.n_rows(); }

  virtual int
  n_ext_parm() const noexcept override
  { return m_nextparm; }

  virtual void
  update(const double *extparm) override;

  void
  calc_rxs(const bos::measure &fu, const bos::measure &fv,
      std::vector<std::pair<double, double>> &out) const override
  { m_base.calc_rxs(fu, fv, out); }

  void
  calc_corrfunc(const std::pair<int, int> &cells,
      std::vector<std::pair<double, double>> &out) const override
  { m_base.calc_corrfunc(cells, out); }

  void
  calc_mult_probs(const int cell, mult_prob_table &out) const override
  { m_base.calc_mult_probs(cell, out); }

  void
  calc_stronglocty_extra(const int cell, mult_prob_table &out) const override
  { m_base.calc_stronglocty_extra(cell, out); }

  double
  symmetry_term() const override
  { return m_base.symmetry_term(); }

  double
  symmetry_term_abs() const override
  { return m_base.symmetry_term_abs(); }

  void
  write(FILE *out) const override
  { m_base.write(out); }

  void
  read(std::istream &in) override
  { m_base.read(in); }

  void
  to_ext_parms(std::vector<double> &extparms) const override
  { abort(); }

  void
  print_table(std::ostream &out) const;

  private:
  void
  update_aux(int xidx, const int *y, int cell, double p);

  protected:
  matrix m_base;
  std::vector<std::vector<std::pair<state, double>>> m_table;

  private:
  int m_nextparm;
}; // class hum_matrix

class spatsym_hum_matrix : public hum_matrix {
  public: 
  spatsym_hum_matrix(const cellular_universe &model);

  virtual
  ~spatsym_hum_matrix() = default;

  int
  n_ext_parm() const noexcept override
  { return m_nextparm; }

  int
  n_ext_parm(int mult) const noexcept
  { return m_classmap[mult].size(); }

  class substate_class {
    public:
    substate_class(const state_set &substates) : m_substates {substates} { }

    bool
    operator () (const state &s) const noexcept
    {
      const dihedral rot = dihedral(3)*dihedral::s;
      const auto end = m_substates.end();
      return m_substates.find(s) != end or m_substates.find(s(rot)) != end;
    }

    bool
    empty() const noexcept
    { return m_substates.empty(); }

    const state&
    representative() const
    {
      if (m_substates.empty())
        throw std::logic_error {"eq. class is empty"};
      return *m_substates.begin();
    }

    private:
    state_set m_substates;
  }; // class substate_class

  const substate_class&
  parm_class(int extparmno) const
  {
    for (int mult = 1; mult <= model().n_balls(); ++mult)
    {
      const int nclassinrow = m_classmap[mult].size();
      if (extparmno < nclassinrow)
        return m_classmap[mult][extparmno].second;
      extparmno -= nclassinrow;
    }
    throw std::logic_error {"no such external parameter"};
    abort();
  }

  const substate_class&
  parm_class(int mult, int extparmidx) const
  {
    const int nclassinrow = m_classmap[mult].size();
    if (extparmidx < nclassinrow)
      return m_classmap[mult][extparmidx].second;
    std::cerr << "> error: no such external parameter: {mult=" << mult
      << ", idx=" << extparmidx << "} (row contains " << m_classmap[mult].size()
      << " elements)" << std::endl;
    throw std::logic_error {"no such external parameter"};
  }

  int
  to_parmno(int mult, int extparmidx) const noexcept
  {
    int rowoffs = 0;
    for (int m = 0; m < mult; ++m)
      rowoffs += m_classmap[m].size();
    return rowoffs + extparmidx;
  }

  int
  to_parmno(int mult, const state &repr) const noexcept
  {
    // row offset
    int rowoffs = 0;
    for (int m = 0; m < mult; ++m)
      rowoffs += m_classmap[m].size();
    // class offset
    int classoffs = 0;
    for (const auto & [_, eqclass]: m_classmap[mult])
    {
      if (eqclass(repr))
        break;
      else
        classoffs ++;
    }
    return rowoffs + classoffs;
  }


  void
  update(const double *extparm) override;

  private:
  std::vector<std::vector<std::pair<std::vector<int>, substate_class>>>
    m_classmap;
  int m_nextparm;
}; // class spatsym_hum_matrix

class spatsym_hum_matrix_simp : public spatsym_hum_matrix {
  public: 
  spatsym_hum_matrix_simp(const cellular_universe &model, int maxchange);

  int
  n_ext_parm() const noexcept override
  { return m_nextparm; }

  void
  update(const double *extparm) override;

  private:
  const int m_maxchange;
  int m_nextparm;
}; // class spatsym_hum_matrix_simp

class spatsym_hum_matrix_noint : public spatsym_hum_matrix {
  public: 
  using spatsym_hum_matrix::spatsym_hum_matrix;
  spatsym_hum_matrix_noint(const cellular_universe &model, int maxchange);

  int
  n_ext_parm() const noexcept override
  { return m_nextparm; }

  void
  update(const double *extparm) override;

  private:
  const int m_maxchange;
  int m_nextparm;
}; // class spatsym_hum_matrix_noint

} // namespace bos

namespace std {
template <>
struct hash<bos::mult_prob_marker> {
  size_t
  operator () (const bos::mult_prob_marker &m) const noexcept
  { return m_statehash(m.substate) + m.nfin*pow(10, m.substate.n_cells()+1); }
  private:
  std::hash<bos::state> m_statehash;
};
}

#endif
