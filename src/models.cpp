#include "boson/models.hpp"

#include "boson/2balls.hpp"
#include "boson/3balls.hpp"
#include "boson/4balls.hpp"
#include "boson/nballs.hpp"

#include <unordered_map>
#include <memory>

const bos::cellular_universe&
bos::select_model(unsigned nballs)
{
  //static const bos::two_balls N2model {6};
  //static const bos::three_balls N3model {6};
  //static const bos::four_balls N4model {6};
  //switch (nballs)
  //{
    //case 2: return N2model;
    //case 3: return N3model;
    //case 4: return N4model;
    //default: throw std::logic_error {"undefined number of balls"};
  //}

  static std::unordered_map<int, std::unique_ptr<bos::n_balls>> cache;
  try
  {
    return *cache.at(nballs);
  }
  catch (const std::out_of_range &_)
  {
    return
      *cache
        .emplace(nballs, std::make_unique<bos::n_balls>(nballs, 6))
        .first->second;
  }
}

// namespace

