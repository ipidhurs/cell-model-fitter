#include "boson/matrix.hpp"


bos::spatsym_matrix::spatsym_matrix(const cellular_universe &model)
: matrix_base {model},
  m_base {model},
  m_idxmap {model.index_map()}
{
  spatial_quotient(model, m_part);

  m_gymap.resize(n_rows());
  for (int iclass = 0; iclass < m_part.size(); ++iclass)
  {
    const int repridx = m_part[iclass][0].first;
    matrix_row &reprrow = m_base.m_rows[repridx];

    for (const auto [xidx, g] : m_part[iclass])
    {
      matrix_row &xrow = m_base.m_rows[xidx];
      m_gymap[xidx].resize(xrow.size());
      for (int irepry = 0; irepry < xrow.size(); ++irepry)
      {
        const state &repry = m_states[reprrow[irepry].state_idx];
        const state &y = repry(g);
        const int yidx = m_idxmap.at(y);
        // now we need to find offset of this `y` in xidx-row
        const auto it = std::find_if(xrow.begin(), xrow.end(),
          [=] (const auto &rowent) -> bool {
            return rowent.state_idx == yidx;
          });
        const int iy = it - xrow.begin();
        m_gymap[xidx][iy] = irepry;
      }
    }
  }
}

void
bos::spatsym_matrix::update(const double *extparm)
{
  for (int iclass = 0; iclass < m_part.size(); ++iclass)
  {
    // representative of the class
    const int repridx = m_part[iclass][0].first;
    matrix_row &reprrow = m_base.m_rows[repridx];

    double s = 1;
    for (int iy = 0; iy < reprrow.size() - 1; s += extparm[iy++]);
    for (int iy = 0, iext = 0; iy < reprrow.size(); ++iy)
    {
      if (reprrow[iy].state_idx == repridx)
        reprrow[iy].val = 1/s;
      else
        reprrow[iy].val = extparm[iext++]/s;
    }

    for (const auto &[xidx, g] : m_part[iclass])
    {
      matrix_row &row = m_base.m_rows[xidx];
      assert(row.size() == reprrow.size());
      for (int iy = 0; iy < row.size(); ++iy)
        row[iy].val = reprrow[m_gymap[xidx][iy]].val;
    }

    // shift parameters
    extparm += reprrow.size() - 1;
  }
}

void
bos::spatsym_matrix::to_ext_parms(std::vector<double> &extparms) const
{
  extparms.clear();
  extparms.reserve(n_ext_parm());
  for (int iclass = 0; iclass < m_part.size(); ++iclass)
  {
    // representative of the class
    const int repridx = m_part[iclass][0].first;
    const matrix_row &reprrow = m_base.m_rows[repridx];

    // find diagonal entry
    int iy_diag = 0;
    for (int iy = 0; iy < reprrow.size(); ++iy)
    {
      if (reprrow[iy].state_idx == repridx)
      {
        iy_diag = iy;
        break;
      }
    }

    // find `s`
    const double s = 1/reprrow[iy_diag].val - 1;

    // calc ext parameters
    for (int iext = 0, iy = 0; iext < reprrow.size() - 1; ++iext, ++iy)
    {
      if (iy == iy_diag)
        iy++;
      extparms.push_back(reprrow[iy].val*(1 + s));
    }
  }
}

