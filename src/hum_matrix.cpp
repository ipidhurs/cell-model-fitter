#include "boson/matrix.hpp"
#include "boson/2balls.hpp"
#include "boson/3balls.hpp"
#include "boson/4balls.hpp"
#include "boson/nballs.hpp"

#include <set>
#include <cstring>

using namespace bos;

hum_matrix::hum_matrix(const cellular_universe &model)
: matrix_base(model),
  m_base {model}
{
  const int nballs = model.n_balls();
  //const int ncells = model.n_cells();

  m_nextparm = 0;
  for (int mult = 0; mult <= nballs; ++mult)
  {
    state_set perms;
    switch (mult)
    {
      case 0:
        break;

      case 1:
      {
        { const std::vector<int> buf {1, 0, 0}; perms.emplace(buf); }
        { const std::vector<int> buf {0, 1, 0}; perms.emplace(buf); }
        { const std::vector<int> buf {0, 0, 1}; perms.emplace(buf); }
        break;
      }

      case 2:
        generate_states_2(3, perms);
        break;

      case 3:
        generate_states_3(3, perms);
        break;

      case 4:
        generate_states_4(3, perms);
        break;

      default:
        generate_states_n(mult, 3, perms);
    }
    m_table.emplace_back();
    std::vector<std::pair<state, double>> &row = m_table.back();
    for (const state &perm : perms)
      row.emplace_back(perm, 0);
    if (not perms.empty())
      m_nextparm += perms.size() - 1;
  }

  //for (int  i = 0; i <= nballs; ++i)
  //{
    //std::cerr << "=== " << i << " goes to:" << std::endl;
    //for (const auto &[suby, _] : m_table[i])
      //std::cerr << "    - " << suby << std::endl;
  //}
}

static int g_indent = 0;

void
hum_matrix::update_aux(int xidx, const int *yin, int cell, double pin)
{
  const int ncells = model().n_cells();
  const state &x = states()[xidx];

  //std::cerr << std::string(g_indent, ' ');
  //std::cerr << "=== update (cell " << cell << "/" << ncells << ", " << x[cell]
    //<< " balls)" << std::endl;

  //g_indent += 2;

  if (m_table[x[cell]].empty())
  {
    if (cell+1 == ncells) // deepest level
    {
      const state &y {yin, ncells};
      std::cerr << std::string(g_indent, ' ');
      //std::cerr << "y = " << y << ", p = " << pin << std::endl;
      const int yidx = model().state_index(y);
      const int iy = m_base.state_index_to_row_entry(xidx, yidx);
      //std::cerr << "=== final probability [" << xidx << "][" << iy << "]: " << p << std::endl;
      m_base.m_rows[xidx][iy].val += pin;
    }
    else // fold with other cells
    {
      update_aux(xidx, yin, cell+1, pin);
    }
  }
  else
  {
    for (const auto &[suby, subp] : m_table[x[cell]])
    {
      // accumulate probability
      const double pout = pin*subp;
      // accumulate final state
      int yout[ncells];
      memcpy(yout, yin, sizeof(int)*ncells);
      yout[(cell+ncells-1) % ncells] += suby[0]; // to the left
      yout[cell] += suby[1]; // not moved
      yout[(cell+1) % ncells] += suby[2]; // to the right

      //std::cerr << std::string(g_indent, ' ');
      //std::cerr << state(yin, ncells) << " + " << x[cell] << "@" << cell << "->"
        //<< suby << "(" << subp << ") = " << state(yout, ncells) << std::endl;

      if (cell+1 == ncells) // deepest level
      {
        const state &y {yout, ncells};
        std::cerr << std::string(g_indent, ' ');
        //std::cerr << "y = " << y << ", p = " << pout << std::endl;
        const int yidx = model().state_index(y);
        const int iy = m_base.state_index_to_row_entry(xidx, yidx);
        //std::cerr << "=== final probability [" << xidx << "][" << iy << "]: " << p << std::endl;
        m_base.m_rows[xidx][iy].val += pout;
      }
      else // fold with other cells
      {
        update_aux(xidx, yout, cell+1, pout);
      }
    }
  }

  //g_indent -= 2;
}

void
hum_matrix::update(const double *extparm)
{
  const int nstates = model().states().size();
  const int ncells = model().n_cells();
  const int nballs = model().n_balls();

  // update table
  for (int mult = 1; mult <= nballs; ++mult)
  {
    double s = 0;
    for (int i = 0; i < m_table[mult].size() - 1; ++i)
      s += extparm[i];

    for (int i = 0, iext = 0; i < m_table[mult].size(); ++i)
    {
      auto &[suby, subp] = m_table[mult][i];
      if (suby[1] == mult) // "diagonal" entry = 1/(1+s)
        subp = 1/(1+s);
      else // "non-diagonal" etnry = ζ/(1+s)
        subp = extparm[iext++]/(1+s);
    }
    extparm += m_table[mult].size() - 1;
  }
  //for (int  i = 0; i <= nballs; ++i)
  //{
    //std::cerr << "=== " << i << " goes to:" << std::endl;
    //for (const auto &[suby, subp] : m_table[i])
      //std::cerr << "    - " << suby << "(" << subp << ")" << std::endl;
  //}

  // update transition matrix
  for (int xidx = 0; xidx < nstates; ++xidx)
  {
    // reset the row
    for (int iy = 0; iy < m_base.row_length(xidx); ++iy)
      m_base.m_rows[xidx][iy].val = 0;
    // calculate probabilities for the row
    int y[ncells];
    std::fill(y, y+ncells, 0);
    update_aux(xidx, y, 0, 1);
  }
}

void
hum_matrix::print_table(std::ostream &out) const
{
  const int nballs = model().n_balls();
  for (int mult = 0; mult <= nballs; ++mult)
  {
    std::cout << "--- mult " << mult << std::endl;
    for (const auto &[substate, p] : m_table[mult])
      std::cout << "    P(" << substate << ") = " << p << std::endl;
  }
}

