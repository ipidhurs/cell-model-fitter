#include "boson/matrix.hpp"

using namespace bos;

spatsym_hum_matrix::spatsym_hum_matrix(const cellular_universe &model)
: hum_matrix(model),
  m_classmap (model.n_balls()+1)
{
  for (int mult = 1; mult <= model.n_balls(); ++mult)
  {
    std::vector<bool> mem (m_table[mult].size()-1, false);

    // find index of "diagonal" substate
    int diagidx = -1;
    for (int isub = 0; isub < m_table[mult].size(); ++isub)
    {
      if (m_table[mult][isub].first[1] == mult)
      {
        diagidx = isub;
        break;
      }
    }
    assert(diagidx >= 0);

    std::vector<std::pair<std::vector<int>, substate_class>>
      &row = m_classmap[mult];
    for (int isub = 0; isub < m_table[mult].size(); ++isub)
    {
      // skip "diagonal" substate
      if (isub == diagidx)
        continue;
      // correct for skip of "diagonal"
      const int isub_corr = isub > diagidx ? isub - 1 : isub;
      // check if already visited
      if (mem[isub_corr])
        continue;
      // not visited => build a new eq-class
      std::vector<int> simsubs;
      state_set eqclass;
      // --
      simsubs.push_back(isub_corr);
      eqclass.emplace(m_table[mult][isub].first);
      mem[isub_corr] = true;
      for (int jsub = isub+1; jsub < m_table[mult].size(); ++jsub)
      {
        // correct for skip of "diagonal"
        const int jsub_corr = jsub > diagidx ? jsub - 1 : jsub;
        // reflect jth substate and check if its equal to the ith one
        const state &substate = m_table[mult][jsub].first;
        const unsigned buf[3] = {substate[2], substate[1], substate[0]};
        if (state(buf, 3) == m_table[mult][isub].first)
        {
          simsubs.push_back(jsub_corr);
          eqclass.insert(substate);
          mem[jsub_corr] = true;
        }
      }
      row.emplace_back(simsubs, eqclass);
    }
  }

  m_nextparm = 0;
  for (int mult = 0; mult <= model.n_balls(); ++mult)
  {
    if (not m_classmap[mult].empty())
      m_nextparm += m_classmap[mult].size();
  }
}

void
spatsym_hum_matrix::update(const double *extparm)
{
  const int nparmout = hum_matrix::n_ext_parm();
  double extparmout[nparmout];
  const double *iter = extparm;
  double *oiter = extparmout;
  int mult = 0;

  //std::cerr << "- - - - - - - - - - - - - - - - - - - - - - - - -" << std::endl;
  //std::cerr << "SPATSYM_HUM_MATRIX PARAMETERS:" << std::endl;
  //for (int i = 0; i < spatsym_hum_matrix::n_ext_parm(); ++i)
    //std::cerr << "[" << i << "] = " << extparm[i] << std::endl;
  //std::cerr << "- - - - - - - - - - - - - - - - - - - - - - - - -" << std::endl;

  for (const auto &classrow : m_classmap)
  {
    for (const auto &eqclass : classrow)
    {
      for (const int isub : eqclass.first)
        oiter[isub] = *iter;
      ++iter;
    }
    if (not m_table[mult].empty())
      oiter += m_table[mult].size() - 1;
    ++mult;
  }
  assert(iter - extparm == spatsym_hum_matrix::n_ext_parm());
  hum_matrix::update(extparmout);
}

spatsym_hum_matrix_simp::spatsym_hum_matrix_simp(const cellular_universe &model,
    int maxchange)
: spatsym_hum_matrix(model),
  m_maxchange {maxchange}
{
  m_nextparm = 0;
  for (int mult = 1; mult <= model.n_balls(); ++mult)
  {
    const int nparminrow = spatsym_hum_matrix::n_ext_parm(mult);
    for (int oparmidx = 0; oparmidx < nparminrow; ++oparmidx)
    {
      const state &repr = parm_class(mult, oparmidx).representative();
      if (mult - repr[1] <= m_maxchange)
        m_nextparm++;
    }
  }
}

void
spatsym_hum_matrix_simp::update(const double *parmin)
{
  const int nparmout = spatsym_hum_matrix::n_ext_parm();
  double parmout[nparmout];
  const double *iiter = parmin;

  for (int mult = 1; mult <= model().n_balls(); ++mult)
  {
    const int nparminrow = spatsym_hum_matrix::n_ext_parm(mult);
    for (int oparmidx = 0; oparmidx < nparminrow; ++oparmidx)
    {
      const auto &eqclass = parm_class(mult, oparmidx);
      const state &repr = eqclass.representative();
      const int oparmno = spatsym_hum_matrix::to_parmno(mult, oparmidx);
      assert(repr[1] != mult);
      parmout[oparmno] = (mult - repr[1] <= m_maxchange) ? *iiter++ : 0;
    }
  }
  spatsym_hum_matrix::update(parmout);
}

spatsym_hum_matrix_noint::spatsym_hum_matrix_noint(
    const cellular_universe &model, int maxchange)
: spatsym_hum_matrix(model),
  m_maxchange {maxchange}
{
  m_nextparm = 0;
  for (int mult = 1; mult <= model.n_balls(); ++mult)
  {
    const int nparminrow = spatsym_hum_matrix::n_ext_parm(mult);
    for (int oparmidx = 0; oparmidx < nparminrow; ++oparmidx)
    {
      const auto &eqclass = parm_class(mult, oparmidx);
      const state &repr = eqclass.representative();
      assert(repr[1] != mult);
      const int dn = mult - repr[1];
      if (dn <= m_maxchange and dn == mult)
        m_nextparm ++;
    }
  }
}

void
spatsym_hum_matrix_noint::update(const double *parmin)
{
  const int nparmout = spatsym_hum_matrix::n_ext_parm();
  double parmout[nparmout];
  const double *iiter = parmin;

  for (int mult = 1; mult <= model().n_balls(); ++mult)
  {
    const int nparminrow = spatsym_hum_matrix::n_ext_parm(mult);
    for (int oparmidx = 0; oparmidx < nparminrow; ++oparmidx)
    {
      const auto &eqclass = parm_class(mult, oparmidx);
      const state &repr = eqclass.representative();
      const int oparmno = spatsym_hum_matrix::to_parmno(mult, oparmidx);
      assert(repr[1] != mult);
      const int dn = mult - repr[1];
      if (dn > m_maxchange)
      {
        parmout[oparmno] = 0;
      }
      else if (dn < mult)
      {
        unsigned buf[3] = {repr[0], 0, repr[2]};
        const int oldparmno = spatsym_hum_matrix::to_parmno(dn, state {buf, 3});
        parmout[oparmno] = parmout[oldparmno];
      }
      else // dn == mult
      {
        //std::cerr << "=== param for " << repr << std::endl;
        parmout[oparmno] = *iiter++;
      }
    }
  }
  spatsym_hum_matrix::update(parmout);
}

