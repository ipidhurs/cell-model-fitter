#include "boson/matrix_factory.hpp"

bos::matrix_base&
bos::matrix_factory::operator () (const std::string &matrixdef,
    const cellular_universe &model) noexcept
{
  int buf;

  if (matrixdef == "default")
  {
    m_cache.push_back(new matrix {model});
    return *m_cache.back();
  }
  else if (matrixdef == "spatsym")
  {
    m_cache.push_back(new spatsym_matrix {model});
    return *m_cache.back();
  }
  else if (matrixdef == "human")
  {
    m_cache.push_back(new hum_matrix {model});
    return *m_cache.back();
  }
  else if (matrixdef == "spatsym-human")
  {
    m_cache.push_back(new spatsym_hum_matrix {model});
    return *m_cache.back();
  }
  else if (sscanf(matrixdef.c_str(), "spatsym-human-simp(%d)", &buf) == 1)
  {
    m_cache.push_back(new spatsym_hum_matrix_simp {model, buf});
    return *m_cache.back();
  }
  else if (sscanf(matrixdef.c_str(), "spatsym-human-noint(%d)", &buf) == 1)
  {
    m_cache.push_back(new spatsym_hum_matrix_noint {model, buf});
    return *m_cache.back();
  }
  else
  {
    std::cerr << "> error: undefined value of matrix definition (" << matrixdef
      << ")" << std::endl;
    exit(EXIT_FAILURE);
  }
}

