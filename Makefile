.PHONY: guard all fit fit-release fit-debug

lib-src := $(wildcard src/*.cpp)
minuit-flags := ./minuit/lib/libminuit.a -lgfortran
opt-flags := -O3
#opt-flags := -Og -ggdb
flags := $(opt-flags) -Wall -Wextra -Werror -Wno-sign-compare -Wno-unused

guard:
	@echo "No-no-no. Write command explicitly please."

all: fit

fit: fit-release

fit-release: Release
	@$(MAKE) -C Release install
	@g++ `root-config --libs` -I `root-config --incdir` -I include check-matrix.cpp Release/CMakeFiles/obj_lib.dir/src/*.o -o checkmat -std=gnu++14

fit-debug: Debug
	@$(MAKE) -C Debug install
	@g++ `root-config --libs` -I `root-config --incdir` -I include check-matrix.cpp Debug/CMakeFiles/obj_lib.dir/src/*.o -o checkmat -std=gnu++14

Release:
	@mkdir $@
	@cmake -S . -B $@ -DCMAKE_INSTALL_PREFIX=. -DCMAKE_BUILD_TYPE=$@

Debug:
	@mkdir $@
	@cmake -S . -B $@ -DCMAKE_INSTALL_PREFIX=. -DCMAKE_BUILD_TYPE=$@
	@g++ `root-config --libs` -I `root-config --incdir` -I include check-matrix.cpp Debug/CMakeFiles/obj_lib.dir/src/*.o -o checkmat -std=gnu++14

